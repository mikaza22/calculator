﻿using System.Diagnostics;
using Xamarin.Forms;

namespace Calculator
{
    public partial class CalculatorPage : ContentPage
    {
        string numberOne;
        string numberTwo;
        string op = "";
        bool status;
        bool start;

        public CalculatorPage()
        {
            InitializeComponent();
            status = false; // jjb: Consider more descriptive names to make debugging easier for yourself as code gets more complex. Your method names all are nice and clear. =)
            start = false; 
        }

        void addNumber(object sender, System.EventArgs e)
        {
            Button button = (Button)sender;

            if (!status && !start) {
                start = true;
                displayIO.Text = "";
                numberOne += button.Text;

            } else if (!status) {
                start = true;
                numberOne += button.Text;
                Debug.WriteLine(numberOne);
            } else {
                numberTwo += button.Text;
                Debug.WriteLine(numberTwo);
            }
            displayIO.Text += button.Text;
        }

        void isOperator(object sender, System.EventArgs e)
        {
            Button button = (Button)sender;

            if (start && op == "") {
                status = true;
                displayIO.Text += button.Text;
                op = button.Text;
            }
        }

        void calculate(object sender, System.EventArgs e)
        {
            switch (op)
            {
                case "+":
                    displayIO.Text = add(long.Parse(numberOne), long.Parse(numberTwo)).ToString();
                    break;
                case "-":
                    displayIO.Text = sub(long.Parse(numberOne), long.Parse(numberTwo)).ToString();
                    break;
                case "x":
                    displayIO.Text = mult(long.Parse(numberOne), long.Parse(numberTwo)).ToString();
                    break;
                case "/":
                    displayIO.Text = div(long.Parse(numberOne), long.Parse(numberTwo)).ToString();
                    break;
                default:
                    displayIO.Text = "0";
                    break;
            }
            reset();
        }

        void clearNumber(object sender, System.EventArgs e)
        {
            displayIO.Text = "0";
            reset();
        }

        private void reset() {
            numberOne = "";
            numberTwo = "";
            op = "";
            start = false;
            status = false;
        }

        private long add(long number1, long number2)
        {
            return (number1 + number2);
        }

        private long sub(long number1, long number2)
        {
            return (number1 - number2);
        }

        private long mult(long number1, long number2)
        {
            return (number1 * number2);
        }

        private long div(long number1, long number2)
        {
            return (number1 / number2);
        }
    }
}
